import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        Integer pluspetit=null;
        if (liste.size()!=0){
        List<Integer> copieliste=new ArrayList<>(liste);
        Collections.sort(copieliste);
        pluspetit=copieliste.get(0);}
        return pluspetit;
    }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        boolean pluspetitque=false;
        List copieliste = new ArrayList(liste);

        if (valeur==Collections.max(copieliste))
            pluspetitque=true;
        return pluspetitque;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
List<T>l=new ArrayList<T>();
	if(liste1.size()<liste2.size()){
for(T elem : liste1){
	for(T elem2 : liste2){
	if(elem==elem2){l.add(elem);}
}
}
}
else{for(T elem : liste2){
	for(T elem2 : liste1){
	if(elem==elem2){l.add(elem);}
}
}
}

	return l;


}

    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
 	List<String> l = new ArrayList<>();
	String mot="";
	for (int i=0 ; i<texte.length();i++){
		if(texte.charAt(i)==' '){
			l.add(mot);
			}
else{mot+=texte.charAt(i);}
}
        return l;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
	List<String> l = decoupe(texte);
	String lettre=l.get(0);
	int cpt1=0;
  int cpt2=0;
	Collections.sort(l);
	if (l.size()==0){return null;}
	 else{for(int i=1;i<l.size();i++){
	    if (l.get(i-1)==l.get(i)){
	       cpt1+=1;}
	else{cpt2=cpt1;
	   cpt1=0;}
if (cpt1>cpt2){lettre=l.get(i);}}
}

        return lettre;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
      int parenteseouv = 0;
      int parentesefrm = 0;
      String[] carac = chaine.split(""); // Je m'aide en décomposant la chaîne sous forme d'index me donnant des "char"
      for (int i=0; i<carac.length; i++)
      {
          if (parentesefrm <= parenteseouv &&  parenteseouv !=0)
              if (carac[i].equals(")"))
                  parentesefrm++;
          if (carac[i].equals("("))
              parenteseouv++;
      }
      return (parentesefrm == parenteseouv && !carac[carac.length-1].equals("("));
  }


    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        return true;

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        return true;
    }



}
